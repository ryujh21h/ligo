module Schema.ListDeclarationsResponse (ListDeclarationsResponse) where

import Data.Text (Text)

-- TODO: Figure out how to make this a newtype and generate a Schema for it
type ListDeclarationsResponse = [Text]
