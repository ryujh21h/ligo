
### SYNOPSIS
```
ligo daemon
```

### DESCRIPTION
Run LIGO subcommands without exiting the process

### FLAGS
**-help**
print this help text and exit (alias: -?)


